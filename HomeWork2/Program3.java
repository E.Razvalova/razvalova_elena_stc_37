//среднее арифметическое элементов массива

import java.util.Scanner;
class Program3{
	 public static void main (String [] arge) {
	  Scanner scanner = new Scanner(System.in);
	  int n = scanner.nextInt();
	  System.out.println();
	  int [] array = new int[n];
	  double arraySum = 0;
	  double arrayAverage = 0;
	  
	  for (int i = 0; i < array.length; i++){
	      array[i] = scanner.nextInt();
	     arraySum += array[i];
	  }
	  arrayAverage = arraySum / n;
	 System.out.println("среднее арифметическое элементов массива = " + arrayAverage);
	}	  
}
