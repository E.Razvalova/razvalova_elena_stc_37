//перемена max и min элементов массива 
import java.util.Scanner;
import java.util.Arrays;
class Program4{
    public static void main(String arge[]){
	   Scanner scanner = new Scanner(System.in);
	   int n = scanner.nextInt();
	   System.out.println();
	   int array[] = new int[n];
	   int min = array[0];
	   int max = array[0];
	   int positionOfMin = 0;
	   int positionOfMax = 0; 
	   int temp;

	    for (int i = 0; i < array.length; i++){
	    	array[i] = scanner.nextInt();
	    	if (array[i] > max) {
            max = array[i];
            positionOfMax = i;
            }
          	if (array[i] < min) {
            min = array[i];
            positionOfMin = i;
            }
        }
        System.out.println("min = " + array[positionOfMin]);
	    System.out.println("max = " + array[positionOfMax]); 

        temp = array[positionOfMin];
        array[positionOfMin] = array[positionOfMax];
        array[positionOfMax] = temp;
	    
        System.out.println(Arrays.toString(array));
     }
} 
