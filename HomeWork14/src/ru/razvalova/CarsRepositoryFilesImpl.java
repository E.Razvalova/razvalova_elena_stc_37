package ru.razvalova;

import java.io.*;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CarsRepositoryFilesImpl implements CarsRepository {
    private String fileName;
    private static final Function<String, Car> carMapper = line -> {
        String parsedLine[] = line.split("#");
        return new Car(Integer.parseInt(parsedLine[0]),
                parsedLine[1],
                parsedLine[2],
                Integer.parseInt(parsedLine[3]),
                Integer.parseInt(parsedLine[4]));
    };

    public CarsRepositoryFilesImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<Car> findAll() {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            List<Car> cars = reader
                    .lines()
                    .map(carMapper)
                    .collect(Collectors.toList());
            reader.close();
            return cars;
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public List<Car> findAllColorBlack(String carColor) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            List<Car> cars = reader
                    .lines()
                    .map(carMapper)
                    .filter(car -> car.getCarColor().equals(carColor))
                    .collect(Collectors.toList());
            reader.close();
            return cars;
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            throw new IllegalStateException(e);

        }
    }
}