package ru.razvalova;

import java.io.IOException;
import java.util.List;

public interface CarsRepository {
    List<Car> findAll() ;
    List<Car> findAllColorBlack(String carColor);
}
