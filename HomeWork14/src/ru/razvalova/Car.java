package ru.razvalova;

import java.util.Objects;

public class Car implements Comparable<Car> {
    private Integer carNumber;
    private String carModel;
    private String carColor;
    private Integer carMileage;
    private Integer CarCost;


    public Car(Integer carNumber, String carModel, String carColor, Integer carMileage, Integer carCost) {
        this.carNumber = carNumber;
        this.carModel = carModel;
        this.carColor = carColor;
        this.carMileage = carMileage;
        this.CarCost = carCost;
    }

    public Integer getCarNumber() {
        return carNumber;
    }

    public String getCarModel() {
        return carModel;
    }

    public String getCarColor() {
        return carColor;
    }

    public Integer getCarMileage() {
        return carMileage;
    }

    public Integer getCarCost() {
        return CarCost;
    }

    public void setCarNumber(Integer carNumber) {
        this.carNumber = carNumber;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public void setCarColor(String carColor) {
        this.carColor = carColor;
    }

    public void setCarMileage(Integer carMileage) {
        this.carMileage = carMileage;
    }

    public void setCarCost(Integer carCost) {
        CarCost = carCost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return Objects.equals(carNumber, car.carNumber) &&
               Objects.equals(carModel, car.carModel) &&
               Objects.equals(carColor, car.carColor) &&
               Objects.equals(carMileage, car.carMileage) &&
               Objects.equals(CarCost, car.CarCost);
    }

    @Override
    public int hashCode() {
        return Objects.hash(carNumber, carModel, carColor, carMileage, CarCost);
    }

    @Override
    public String toString() {
        return "Car{" +
                "carNumber=" + carNumber +
                ", carModel='" + carModel + '\'' +
                ", carColor='" + carColor + '\'' +
                ", carMileage=" + carMileage +
                ", CarCost=" + CarCost +
                '}';
    }

    @Override
    public int compareTo(Car o) {
        return 0;
    }
}


