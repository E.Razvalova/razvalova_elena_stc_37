package ru.razvalova;

public class Main {

    public static void main(String[] args) {
        // создал список
       /* InnoList list = new InnoLinkedList();

        list.add(7);
        list.add(8);
        list.add(10);
        list.add(12);
        list.add(15);
        list.add(20);
        list.add(-77);
        list.add(100);*/

        InnoList list1 = new InnoArrayList();
        list1.add (1);
        list1.add(2);
        list1.addToBegin (10);

        System.out.println(list1.add (1)); // 7
        System.out.println(list1.get(3)); // 12
        System.out.println(list1.get(7)); // 100

        InnoIterator iterator = list1.iterator();

        while (iterator.hasNext()) {
            System.out.println(iterator.next() + " ");
        }

        InnoIterator iterator2 = list1.iterator();

        while (iterator2.hasNext()) {
            System.out.println(iterator2.next() + " ");
        }

    }
}
