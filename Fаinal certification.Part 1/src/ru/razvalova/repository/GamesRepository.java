package ru.razvalova.repository;

import ru.razvalova.models.Game;

public interface GamesRepository {
    void save(Game game);
    Game findById(Long gameId);
    void update(Game game);
    }

