package ru.razvalova.repository;

import ru.razvalova.models.Player;

public interface PlayersRepository {
    Player findByNickname(String nickname);

    void save(Player player);

    void update(Player player) ;

}


