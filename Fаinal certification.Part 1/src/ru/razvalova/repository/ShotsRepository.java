package ru.razvalova.repository;

import ru.razvalova.models.Shot;

public interface ShotsRepository {
    void save(Shot shot);
}

