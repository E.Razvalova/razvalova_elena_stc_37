package ru.razvalova.servise;
import ru.razvalova.repository.GamesRepository;
import ru.razvalova.repository.PlayersRepository;
import ru.razvalova.repository.ShotsRepository;
import ru.razvalova.models.Game;
import ru.razvalova.models.Player;
import ru.razvalova.models.Shot;
import java.time.LocalDateTime;

public class GameServiceImpl implements GameService {

    private PlayersRepository playersRepository;

    private GamesRepository gamesRepository;

    private ShotsRepository shotsRepository;

    public GameServiceImpl(PlayersRepository playersRepository, GamesRepository gamesRepository, ShotsRepository shotsRepository) {
        this.playersRepository = playersRepository;
        this.gamesRepository = gamesRepository;
        this.shotsRepository = shotsRepository;
    }

    @Override
    public Long startGame(String firstIp, String secondIp, String firstPlayerNickname, String secondPlayerNickname) {
        // получили информацию об обоих игроках
        Player first = checkIfExists(firstIp, firstPlayerNickname);
        Player second = checkIfExists(secondIp, secondPlayerNickname);
        // создали игру
        Game game = new Game(LocalDateTime.now(), first, second, 0, 0, 0L);
        // сохранили игру в репозитории
        gamesRepository.save(game);
        return game.getId();
    }

    private Player checkIfExists(String ip, String nickname) {
        Player player = playersRepository.findByNickname(nickname);
        // если нет первого игрока под таким именем
        if (player == null) {
            // создаем игрока
            player = new Player(ip, nickname, 0, 0, 0);
            // сохраняем его в репозитории
            playersRepository.save(player);
        } else {
            // если такой игрок был -> обновляем у него IP-адрес
            player.setIp(ip);
            playersRepository.update(player);
        }

        return player;
    }

    @Override
    public void shot(Long gameId, String shooterNickname, String targetNickname) {
        Player shooter = playersRepository.findByNickname(shooterNickname);
        Player target = playersRepository.findByNickname(targetNickname);
        Game game = gamesRepository.findById(gameId);
        Shot shot = new Shot(LocalDateTime.now(), game, shooter, target);

        shooter.setPoints(shooter.getPoints() + 1);

        if (game.getPlayerFirst().getName().equals(shooterNickname)) {
            game.setPlayerFirstShotsCount(game.getPlayerFirstShotsCount() + 1);
        }

        if (game.getPlayerSecond().getName().equals(shooterNickname)) {
            game.setPlayerSecondShotsCount(game.getPlayerSecondShotsCount() + 1);
        }

        playersRepository.update(shooter);
        gamesRepository.update(game);
        shotsRepository.save(shot);
    }


}

