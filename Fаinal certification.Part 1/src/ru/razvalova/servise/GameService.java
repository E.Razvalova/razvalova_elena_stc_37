package ru.razvalova.servise;

public interface GameService {
    Long startGame(String firstIp, String secondIp, String firstPlayerNickname, String secondPlayerNickname);
    void shot(Long gameId, String shooterNickname, String targetNickname);
}
