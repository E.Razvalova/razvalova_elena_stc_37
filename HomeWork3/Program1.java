import java.util.Scanner;
import java.util.Arrays;
class Program1{

	 public static int[] inputArray () {
	    Scanner scanner = new Scanner(System.in);
	    int n = scanner.nextInt();
	    System.out.println();
 	    int [] array = new int[n];
 	   	
 	    for (int i = 0; i < array.length; i++){
   	   		array[i] = scanner.nextInt();
        }
        return array;
    }

   	public static int printArraySum(int[]array) {
   	   	int arraySum = 0;
   	   	for (int i = 0; i < array.length; i++){
   	   		arraySum += array[i];
	    }
	    System.out.println ();
	    return  arraySum;
	}
 
    public static double printArrayAverage(int[]array) {
   	   	double arraySum1 = 0;
   	   	double arrayAverage;
   	   	for (int i = 0; i < array.length; i++){
   	   		arraySum1 += array[i];
	      }
	    arrayAverage = arraySum1 / array.length; 
	    System.out.println ();
	    return  arrayAverage;
	}
 
    public static int printArrayToNumber(int[]array) {
        int arrayNumber = array[0];
        for (int i = 1; i < array.length; i++){
        	arrayNumber = arrayNumber * 10 + array[i];
        }
        System.out.println (  );
        return  arrayNumber;
    }
 

	public static void printExpendedArray(int[]array) {
   	   	int temp;
   	   	System.out.println("Expended Array");
   	   	for (int i = 0; i < array.length / 2; i++){
			temp = array[i];
			array[i] = array[array.length - i - 1];
			array[array.length - i - 1] = temp; 
			}
		for (int i = 0; i < array.length; i++){
			System.out.print (array[i] + "  ");
		}
    System.out.println( );
    }

    public static void changeMaxToMin (int[]array){
    	System.out.println("Change max and min: ");
        int min = array[0];
	    int max = array[0];
	    int positionOfMin = 0;
	    int positionOfMax = 0; 
	    int temp;

	    for (int i = 0; i < array.length; i++){
	      	if (array[i] > max) {
            max = array[i];
            positionOfMax = i;
            }
          	if (array[i] < min) {
            min = array[i];
            positionOfMin = i;
            }
        }
        System.out.println("min = " + array[positionOfMin]);
	    System.out.println("max = " + array[positionOfMax]); 

        temp = array[positionOfMin];
        array[positionOfMin] = array[positionOfMax];
        array[positionOfMax] = temp;
	    
        System.out.println(Arrays.toString(array));
    }
 
    public static void sortBubble (int[]array){
    System.out.println("Bubble sort ");
    boolean isSorted = false;
    int temp2;
        while(!isSorted) {
             isSorted = true;
            for (int i = 0; i < array.length-1; i++) {
                if(array[i] > array[i+1]){
                    isSorted = false;
 
                    temp2 = array[i];
                    array[i] = array[i+1];
                    array[i+1] = temp2;
                }
            }
        }
        System.out.println(Arrays.toString(array));	
    }
    
    public static void main (String[]arge) {
	    int[]array = inputArray();
	    
        int result1 = printArraySum(array) ;
 	      double result2 = printArrayAverage(array);
        int result3 =  printArrayToNumber(array);
   	    System.out.println ( "Array sum = " + result1);
   	    System.out.println();
 	      System.out.println ( "Array average = " + result2);
 	      System.out.println();
        System.out.println ( "Array to number = " + result3);
        System.out.println();
        changeMaxToMin (array); 
        System.out.println();
        printExpendedArray (array);
        System.out.println();
        sortBubble (array);
        
 	}  
}
