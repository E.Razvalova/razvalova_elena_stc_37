import java.util.Scanner;
class Program2{
	public static int binarySearch(int[] array, int left,int right, int element) {
			if (right >= left ){
		    int middle = left + (right - left)/ 2;
		 	if (array[middle] == element){
		 		return middle;
		 	}
		 	if (array[middle] > element)
		 		return binarySearch (array, left, middle - 1, element);
		 		return binarySearch (array, middle + 1 , right, element);
            }
		return -1; 
    }
        
    public static void main (String[]arge) {
	    Scanner scanner = new Scanner(System.in); 
	    
	    System.out.println("enter array size: ");
	    int n = scanner.nextInt();
	   	  
	    System.out.println("enter array: ");
	    int[] array = new int[n];
	    

	    for (int i = 0; i < array.length; i++){
			array[i] = scanner.nextInt();
	    }

	    System.out.println("enter element you are looking for: ");
	    int element = scanner.nextInt();


	    int index = binarySearch (array, 0, n-1, element);
	    if (index == -1)
	    	System.out.println ("Element not present");
	    else
   		    System.out.println ("Element found at index: " + index );
   	}	
}


