// фибоначчи
class Program3{
 
    public static int fib(int n, int a, int b){
        if (n <= 2){ 
             return a + b;
         }
         return fib(n - 1, b, a + b);
    }
    public static void main (String[] args) {
        for (int i = 1; i < 15; i++) {
                System.out.println(fib(i, 0, 1));
        }
    }
}