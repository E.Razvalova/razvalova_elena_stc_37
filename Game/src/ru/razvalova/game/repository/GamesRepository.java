package ru.razvalova.game.repository;
import ru.razvalova.game.models.Game;


public interface GameRepository extends CrudRepository<Game>{
    Game findById (Long gameId);
    void update(Game game);
}
