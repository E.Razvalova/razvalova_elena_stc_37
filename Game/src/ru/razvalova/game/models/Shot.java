package ru.razvalova.game.modeles;

import java.time.LocalDateTime;
import java.util.Objects;

public class Shot {
    private Long id;
    private LocalDateTime dateTime;
    private Game game;
    private Player shooter;
    public Player target;

    public Shot(LocalDateTime dateTime, Game game, Player shooter, Player target) {
        this.dateTime = dateTime;
        this.game = game;
        this.shooter = shooter;
        this.target = target;
    }

    public Long getId() {
        return id;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public Game getGame() {
        return game;
    }

    public Player getShooter() {
        return shooter;
    }

    public Player getTarget() {
        return target;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public void setShooter(Player shooter) {
        this.shooter = shooter;
    }

    public void setTarget(Player target) {
        this.target = target;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Shot shot = (Shot) o;
        return Objects.equals(dateTime, shot.dateTime) && Objects.equals(game, shot.game) && Objects.equals(shooter, shot.shooter) && Objects.equals(target, shot.target);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dateTime, game, shooter, target);
    }

    @Override
    public String toString() {
        return "Shot{" +
                "id=" + id +
                ", dateTime=" + dateTime +
                ", game=" + game +
                ", shooter=" + shooter +
                ", target=" + target +
                '}';
    }
}
