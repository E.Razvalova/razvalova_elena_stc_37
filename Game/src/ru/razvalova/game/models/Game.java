package ru.razvalova.game.modeles;

import java.time.LocalDateTime;
import java.util.Objects;

public class Game {
    private Long id;
    private LocalDateTime dateTime;
    private Player playerFirst;
    private Player playerSecond;
    private Integer playerFirstShotsCount;
    private  Integer playerSecondShotsCount;
    private Long secondsGameTimeAmount;

    public Game(LocalDateTime dateTime, Player playerFirst, Player playerSecond, Integer playerFirstShotsCount, Integer playerSecondShotsCount, Long secondsGameTimeAmount) {
        this.dateTime = dateTime;
        this.playerFirst = playerFirst;
        this.playerSecond = playerSecond;
        this.playerFirstShotsCount = playerFirstShotsCount;
        this.playerSecondShotsCount = playerSecondShotsCount;
        this.secondsGameTimeAmount = secondsGameTimeAmount;
    }

    public Long getId() {
        return id;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public Player getPlayerFirst() {
        return playerFirst;
    }

    public Player getPlayerSecond() {
        return playerSecond;
    }

    public Integer getPlayerFirstShotsCount() {
        return playerFirstShotsCount;
    }

    public Integer getPlayerSecondShotsCount() {
        return playerSecondShotsCount;
    }

    public Long getSecondsGameTimeAmount() {
        return secondsGameTimeAmount;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public void setPlayerFirst(Player playerFirst) {
        this.playerFirst = playerFirst;
    }

    public void setPlayerSecond(Player playerSecond) {
        this.playerSecond = playerSecond;
    }

    public void setPlayerFirstShotsCount(Integer playerFirstShotsCount) {
        this.playerFirstShotsCount = playerFirstShotsCount;
    }

    public void setPlayerSecondShotsCount(Integer playerSecondShotsCount) {
        this.playerSecondShotsCount = playerSecondShotsCount;
    }

    public void setSecondsGameTimeAmount(Long secondsGameTimeAmount) {
        this.secondsGameTimeAmount = secondsGameTimeAmount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Game game = (Game) o;
        return Objects.equals(dateTime, game.dateTime) && Objects.equals(playerFirst, game.playerFirst) && Objects.equals(playerSecond, game.playerSecond) && Objects.equals(playerFirstShotsCount, game.playerFirstShotsCount) && Objects.equals(playerSecondShotsCount, game.playerSecondShotsCount) && Objects.equals(secondsGameTimeAmount, game.secondsGameTimeAmount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dateTime, playerFirst, playerSecond, playerFirstShotsCount, playerSecondShotsCount, secondsGameTimeAmount);
    }
}
