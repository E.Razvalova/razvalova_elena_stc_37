import javafx.scene.shape.Circle;

public class Main {

    public static void main(String[] args) {

        //создаём экземпляры класса
        Square square = new  Square ("квадрат", 25);
        square.calculationArea();
        square.calculationPerimeter();
        square.doRelocatable(155,12);
        square.doScalable(5);
        System.out.println("-----------------------------------------");

        Ellipse ellipse = new Ellipse("эллипс", 6,3);
        ellipse.calculationArea();
        ellipse.calculationPerimeter();
        ellipse.doRelocatable(2,5);
        ellipse.doScalable(2);
        System.out.println("-----------------------------------------");

        Rectangle rectangle = new Rectangle("прямоугольник", 10,20 );
        rectangle.calculationArea();
        rectangle.calculationPerimeter();
        rectangle.doRelocatable(10,15);
        rectangle.doScalable(3);
        System.out.println("-----------------------------------------");

        Round round = new Round("круг", 10);
        round.calculationArea();
        round.calculationPerimeter();
        round.doRelocatable(25, 25);
        round.doScalable(25);
        System.out.println("-----------------------------------------");
    }
}

