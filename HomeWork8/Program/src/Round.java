public class Round extends GeometricFigures implements Relocatable, Scalable {
    private int radius;

    public Round(String name, int radius) {
        super(name);
        this.radius = radius;
    }

    @Override
    public void calculationArea() {
        double S = 3.14 * radius * radius;
        String result = String.format("%8.2f", S);
        System.out.println("S круга = " + result);
        System.out.println();
    }

    @Override
    public void calculationPerimeter() {
        double P = 3.14 * 2 * radius;
        String result = String.format("%8.2f", P);
        System.out.println("P круга = " + result);
        System.out.println();
    }


    public void doRelocatable() {
    }

    @Override
    public void doScalable(int c) {
        System.out.println(name + " с радиусом " + radius + " был изменён. ");
        radius = c * radius;
        System.out.println("Увеличили в " + c + " раза, и итоговый радиус стал  " + radius);
        System.out.println();
    }

    public int getRadius() {
        return radius;
    }
}


