public class Ellipse extends GeometricFigures implements Relocatable, Scalable {

    double PI = Math.PI;
    protected int bigSemiAxis;
    protected int smallSemiAxis;

    //конструктор
    public Ellipse(String name, int bigSemiAxis, int smallSemiAxis) {
        super(name);
        this.bigSemiAxis = bigSemiAxis;
        this.smallSemiAxis = smallSemiAxis;
    }

    //переопределяем методы
    @Override
    public void calculationArea() {
        double S = PI * bigSemiAxis * smallSemiAxis;
        String result = String.format("%8.2f", S);
        System.out.println("S эллипса = " + result);
        System.out.println();
    }

    @Override
    public void calculationPerimeter() {
        double P = 2 * PI * Math.sqrt((bigSemiAxis * bigSemiAxis + smallSemiAxis * smallSemiAxis) / 2);
        String result = String.format("%8.2f", P);
        System.out.println("P эллипса = " + result);
        System.out.println();
    }

    //@Override
    public void doRelocatable() {
    }

    @Override
    public void doScalable(int c) {
        System.out.println(name + " с большой полуосью = " + bigSemiAxis + " и малой полуосью = " + smallSemiAxis + " был изменён. ");
        bigSemiAxis = c * bigSemiAxis;
        smallSemiAxis = c * smallSemiAxis;
        System.out.println("Увеличили в " + c + " раза, и итоговый размер стал следующим: большая полуось = " +
                bigSemiAxis + ", малая полуось = " + smallSemiAxis);
        System.out.println();
    }

    public int getBigSemiAxis() {
        return bigSemiAxis;
    }

    public int getSmallSemiAxis() {
        return smallSemiAxis;
    }
}

