public class Square extends GeometricFigures implements Relocatable, Scalable {
    private int size;

    public Square(String name, int size) {
        super(name);
        this.size = size;
    }

    @Override
    public void calculationArea() {
        int S = size * size;
        System.out.println("S квадрата = " + S);
        System.out.println();
    }

    @Override
    public void calculationPerimeter() {
        int P = 4 * size;
        System.out.println("P квадрата = " + P);
        System.out.println();
    }

    public void doRelocatable() {
    }

    @Override
    public void doScalable(int c) {
        System.out.println("Квадрат со сторонами " + size + " был изменен.");
        size = c * size;

        System.out.println("Его увеличили в " + c + " раза. Итоговый размер стал " + size + " на " + size);
    }
}


