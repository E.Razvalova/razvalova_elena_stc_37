public class Rectangle extends GeometricFigures implements Relocatable, Scalable {
    private int longSize;
    private int shortSize;

    public Rectangle(String name, int longSize, int shortSize) {
        super(name);
        this.longSize = longSize;
        this.shortSize = shortSize;
    }

    @Override
    public void calculationArea() {
        int S = longSize * shortSize;
        System.out.println("S прямоугольника = " + S);
        System.out.println();
    }

    @Override
    public void calculationPerimeter() {
        int P = 2 * (longSize + shortSize);
        System.out.println("P прямоугольника = " + P);
        System.out.println();
    }

    public void doRelocatable() {
    }

    @Override
    public void doScalable(int c) {
        System.out.println("Прямоугольник со сторонами " + longSize + " и " + shortSize + " был изменен.");
        longSize = c * longSize;
        shortSize = c * shortSize;
        System.out.println("Его увеличили в " + c + " раза. Итоговый размер стал " + longSize + " и " + shortSize);
    }
}
