public class GeometricFigures implements Relocatable, Scalable {
    protected String name;
    protected int x; // координаты асциссы цетра
    protected int y; // координаты ординаты центра
    protected int a; //размер перемещения по оси абсцисс
    protected int b; //размер перемещения по оси ординат
    protected int c; //коэфициент изменения геометрической фигуры

    public GeometricFigures(String name) {
        this.name = name;
    }

    public void calculationArea() {
    }

    public void calculationPerimeter() {
    }

    @Override
    public void doRelocatable(int a, int b) {
        System.out.println("Координаты центра геометрической фигуры " + name + "(" + x + ";" + y + ")");
        x = x + a;
        y = y + b;
        System.out.println(name + " переместили.");
        System.out.println("Новые координаты центра (" + x + ";" + y + ")");
        System.out.println();
    }

    @Override
    public void doScalable(int c) {
    }

    public String getName() {
        return name;
    }
}
