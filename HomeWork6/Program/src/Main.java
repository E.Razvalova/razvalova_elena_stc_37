import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        //создаем массив программ
        Program program1 = new Program("программа 1.1 ", " транслируется утром ");
        Program program2 = new Program("программа 1.2 ", " транслируется днём ");
        Program program3 = new Program("программа 1.3 ", " транслируется вечером ");
        Program program4 = new Program("программа 1.4 ", " транслируется ночью ");

        Program program5 = new Program("программа 2.1 ", "  транслируется утром ");
        Program program6 = new Program("программа 2.2 ", " транслируется днём ");
        Program program7 = new Program("программа 2.3 ", " транслируется вечером ");
        Program program8 = new Program("программа 2.4 ", " транслируется ночью ");

        Program program9 = new Program("программа 3.1 ", " транслируется утром ");
        Program program10 = new Program("программа 3.2 ", " транслируется днём ");
        Program program11 = new Program("программа 3.3 ", " транслируется вечером ");
        Program program12 = new Program("программа 3.4 ", " транслируется ночью ");

        //создали массив каналов
        Channel channel1 = new Channel("первый", 1);
        Channel channel2 = new Channel("второй", 2);
        Channel channel3 = new Channel("третий", 3);

        //присваиваем программы каналам
        channel1.letProgramIn(program1);
        channel1.letProgramIn(program2);
        channel1.letProgramIn(program3);
        channel1.letProgramIn(program4);

        channel2.letProgramIn(program5);
        channel2.letProgramIn(program6);
        channel2.letProgramIn(program7);
        channel2.letProgramIn(program8);

        channel3.letProgramIn(program9);
        channel3.letProgramIn(program10);
        channel3.letProgramIn(program11);
        channel3.letProgramIn(program12);

        //создали tv с каналами
        Tv tv = new Tv("LG", "2018");

        tv.letChannelIn(channel1);
        tv.letChannelIn(channel2);
        tv.letChannelIn(channel3);

        //создали пульт
        RemoteController remoteController = new RemoteController("lg");

        //назначил пульту tv
        remoteController.goToTv(tv);

        // включаем tv
        remoteController.pushTurnOn();

        // выбираем канал и произвольную передачу
        remoteController.pushSelectChannel();



    }
}

