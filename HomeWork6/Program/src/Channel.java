import java.util.Arrays;
import java.util.Random;

public class Channel {
    private static final Random random = new Random();
    private String nameChannel;
    private int numberChannel;

    //ссылка на tv, на котором существует канал
    private Tv tv;

    //ссылка на массив программ, которые есть на этом канале
    private Program programs[];
    private int programCount;

    public Channel(String nameChannel, int numberChannel) {
        this.nameChannel = nameChannel;
        this.numberChannel = numberChannel;
        this.programs = new Program[4];
        this.programCount = 0;
    }

    // метод , который присваивает программу каналу
    public void letProgramIn(Program program) {
        this.programs[programCount] = program;
        programCount++;
    }

    //метод выбора случайной программы
    public Program selectRandomProgram() {
        return programs[random.nextInt(4)];
    }

    public String getNameChannel() {
        return nameChannel;
    }

    public int getNumberChannel() {
        return numberChannel;
    }

}