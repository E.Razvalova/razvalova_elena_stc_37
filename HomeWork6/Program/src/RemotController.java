import java.util.Scanner;
import java.util.Random;

 class RemoteController {
    private String modelRemoteController;

    //ссылка на tv, которым пульт управляет
    private Tv tv;

    public RemoteController(String modelRemoteController) {
        this.modelRemoteController = modelRemoteController;
        //   this.channel = new Channel[3];
        //   this.channelCount = 0;
    }

    //метод который назначает пульт телевизору
    public void goToTv(Tv tv) {
        this.tv = tv;

        //назначаем пульту самого себя
        this.tv.setRemoteController(this);
    }

    // включаем tv
    public void pushTurnOn() {
        this.tv.turnTv();
    }

    // выбираем канал и случайную программу канала
    public void pushSelectChannel() {
        this.tv.selectChannel();
    }


    public String getModelRemoteController() {
        return modelRemoteController;
    }
}
