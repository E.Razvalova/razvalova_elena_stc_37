import com.sun.org.apache.xpath.internal.objects.XNumber;
import com.sun.org.apache.xpath.internal.objects.XString;

import java.util.Arrays;
import java.util.Random;

import java.util.Scanner;
import java.util.Random;

public class Tv {
    private String manufacturerTv;
    private String modelTv;
    private RemoteController remoteController;

    //массив каналов, которые есть на этом tv
    private Channel channel[];
    private int channelCount;

    //конструктор
    public Tv(String manufacturerTv, String modelTv) {
        this.manufacturerTv = manufacturerTv;
        this.modelTv = modelTv;
        this.channel = new Channel[3];
        this.channelCount = 0;
    }

    //метод который указывает пульт от tv
    public void setRemoteController(RemoteController remoteController) {
        this.remoteController = remoteController;
    }

    //метод, который  выбирает канал c консоли
    public void selectChannel() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("введите номер канала от 1 до 3: ");
        int n = scanner.nextInt();
        if (n <= 3 && n > 0 ) {
            System.out.println("Выбранный Вами канал - " + channel[n-1].getNameChannel());
            System.out.println ("Случайная программа данного канала - " + channel[n-1].selectRandomProgram());

        }
        else {
            System.err.println("трансляция такого канала не настроена");
        }
    }


    //метод, который присваивает канал этому tv
    public void letChannelIn(Channel channel) {
        this.channel[channelCount] = channel;
        channelCount++;
    }

    //метод, который включает tv можно создать
    public void turnTv() {

        System.out.println("телевизор " + manufacturerTv + " включен");
    }

}