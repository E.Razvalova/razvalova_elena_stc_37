import java.util.Random;

public class Program {
    private String nameProgram;
    private String timeProgram;

    //ссылка канал, на котором существует программа
    private Channel channel;

    //конструктор
    public Program(String nameProgram, String timeProgram) {
        this.nameProgram = nameProgram;
        this.timeProgram = timeProgram;
    }

    //метод который назначает программу данному каналу
    public void letProgramInChannel(Channel channel) {
        this.channel = channel;
        //назначаем программе саму себя
        this.channel.letProgramIn(this);
    }

    public String getNameProgram() {

        return nameProgram;
    }

    public String getTimeProgram() {

        return timeProgram;
    }

   @Override
    public String toString() {
        return "Название программы: " + '\'' +  nameProgram + '\'' +
                ", Время выхода: " +  '\'' + timeProgram + '\'' ;
    }
}
