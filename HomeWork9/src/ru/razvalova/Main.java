package ru.razvalova;

public class Main {
    public static void main(String[] args) {

        NumberProcess numberProcess1 = number -> {
            int reversNumber = 0;
            while (number > 0) {
                reversNumber *= 10;
                reversNumber += number % 10;
                number /= 10;
            }
            System.out.println(reversNumber);
        };

        NumberProcess numberProcess2 = number -> {
            String oldStr = Integer.toString(number);
            String newString = oldStr.replace('0', ' ');
            System.out.println(newString.replace(" ", ""));
        };

        NumberProcess numberProcess3 = number -> {
            int num = Integer.parseInt(Integer.toString(number).
                    replace("1", "0").
                    replace("3", "2").
                    replace("5", "4").
                    replace("7", "6").
                    replace("9", "8"));
            System.out.println(num);
        };

        StringProcess stringProcess1 = str -> {
            String reversedStr = new StringBuffer(str).reverse().toString();
            System.out.println(reversedStr);
            return reversedStr;
        };

        StringProcess stringProcess2 = str -> {
            str = str.replaceAll("\\d", "");
            System.out.println(str);
            return str;
        };

        StringProcess stringProcess3 = str -> {
            System.out.println(str.toUpperCase());
            return str;
        };

        numberProcess1.process(123);
        numberProcess2.process(20070);
        numberProcess3.process(5978);

        stringProcess1.process("Hello,World");
        stringProcess2.process("Hel12lo, Wo235rld");
        stringProcess3.process("hello, world");
    }
}