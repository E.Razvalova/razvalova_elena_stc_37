package ru.razvalova;

public interface NumberProcess {
    void process(int number);
}
