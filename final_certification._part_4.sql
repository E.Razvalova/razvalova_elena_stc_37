--создание  таблицы игроков
create table player(
  id bigserial primary key, -- идентификатор игрока
  ip varchar(255), --ip игрока
  name varchar(255), -- имя игрока
  max_points int,  -- количество очков
  wins_count int, --  количество побед игрока
 loses_count int -- количество поражений игрока
  );

--создание таблицы игр
create table game(
 id bigserial primary key ,-- идентификатор игры (bigserial  генерирует автоинкремент, сам новый id)
 data_time timestamp, --дата игры
 player_first bigint, --первый игрок
 foreign key (player_first) references player(id),
 player_second bigint, --второй игрок
 foreign key (player_second) references player(id),
 player_first_shots_count int, --количество выстрелов первого игрока
 player_second_shorts_count int, --количество выстрелов второго игрока
 second_game_time_amount bigint -- время прохождения игры
);

--создание таблицы выстрелов
create table shot (
  id bigserial primary key , -- id выстрела
  data_time timestamp, --время выстрела
  game bigint, --игра, в которой был выстрел
  foreign key (game) references game(id),
  player_shooter bigint, --игрок, который стрелял
  foreign key (player_shooter) references player(id),
  player_target bigint,-- игрок , в которого стреляли
  foreign key (player_target) references player(id)
);
